package br.com.itau.investimentocliente.services

import br.com.itau.investimentocliente.models.Cliente
import br.com.itau.investimentocliente.repositories.ClienteRepository
import spock.lang.Specification

class ClienteServiceTest extends Specification {

	ClienteService clienteService

	ClienteRepository clienteRepository

	def setup() {
		clienteService = new ClienteService()
		clienteRepository = Mock()

		clienteService.clienteRepository = clienteRepository
	}

	def 'deve salvar um cliente'() {
		given: 'os dados de um cliente são informados'
		Cliente cliente = new Cliente()
		cliente.nome = 'José'
		cliente.cpf = '123.123.123-12'

		when: 'o cliente é salvo'
		def clienteSalvo = clienteService.cadastrar(cliente)

		then: 'retorne um cliente'
		1 * clienteRepository.save(_) >> cliente
		clienteSalvo != null
	}

	def 'deve buscar um cliente pelo CPF'() {
		given: 'os dados do cliente existem na base'
		String cpf = '123.123.123-12'
		Cliente cliente = new Cliente()
		cliente.nome = 'José'
		cliente.cpf = cpf
		def clienteOptional = Optional.of(cliente)

		when: 'é feito uma busca informando o cpf'
		def clienteEncontrado = clienteService.buscar(cpf)

		then: 'retorne o cliente buscado'
		1 * clienteRepository.findByCpf(_) >> clienteOptional
		clienteEncontrado.isPresent()
	}
}

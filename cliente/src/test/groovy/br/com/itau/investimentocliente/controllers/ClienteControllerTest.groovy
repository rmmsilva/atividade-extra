package br.com.itau.investimentocliente.controllers

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc

import br.com.itau.investimentocliente.models.Cliente
import br.com.itau.investimentocliente.repositories.ClienteRepository
import br.com.itau.investimentocliente.services.ClienteService
import spock.lang.Specification
import spock.mock.DetachedMockFactory

@WebMvcTest
class ClienteControllerTest extends Specification {
	@Autowired
	MockMvc mockMvc

	@Autowired
	ClienteService clienteService

	@Autowired
	ClienteRepository clienteRepository

	@TestConfiguration
	static class MockConfig {
		def factory = new DetachedMockFactory() {

			@Bean
			ClienteService clienteService() {
				return factory.Mock(ClienteService)
			}

			@Bean
			ClienteRepository clienteRepository() {
				return factory.Mock(ClienteRepository)
			}
		}
	}

	def 'deve buscar um cliente por CPF'() {
		given: 'um cliente existe na base'
		String cpf = '123.123.123-12'
		Cliente cliente = new Cliente()
		cliente.nome = 'José'
		cliente.cpf = cpf
		Optional clienteOptional = Optional.of(cliente)

		when: 'uma busca é realizada'
		def resposta = mockMvc.perform(get('/123.123.123-12'))

		then: 'retorne um cliente'
		1 * clienteService.buscar(_) >> clienteOptional
		clienteOptional.isPresent()
	}

	def 'deve inserir um novo cliente'() {
		given: 'os dados de um cliente são informados'
		Cliente cliente = new Cliente()
		cliente.nome = 'José'
		cliente.cpf = '123.123.123-12'

		when: 'um novo cadastro é realizado'
		def resposta = mockMvc.perform(
				post('/')
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content('{"nome": "José", "cpf": "123.123.123-12"}'))

		then: 'insira o cliente corretamente'
		1 * clienteService.cadastrar(_) >> cliente
		resposta.andExpect(status().isCreated())
				.andExpect(jsonPath('$.nome').value('José'))
	}
}
